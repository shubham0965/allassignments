function longestWord(str)
{
	var strArray=[];
	strArray=str.split(" "); 
	var index=0;
	var max=0;
	for(let i=0;i<strArray.length;i++)
	{
		var count=0;
		for(let j=0;j<strArray[i].length;j++)
		{
			count++;
		}
		if(count>max)
		{
			max=count;
			index=i;
		}
	}
	return strArray[index];
}

console.log(longestWord("abc pQRSTu nmp"));