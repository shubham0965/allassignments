function reverseNumber(num)
{
	num=num+""; //converting number into string 
	return num.split("").reverse().join(""); 
	//split is used to split the string and convert it to array of char
	//reverse will reverse that array
	//it will join all element of array into string
}

console.log(Number(reverseNumber(153)));
//number constructor is used to convert other datatype into number
