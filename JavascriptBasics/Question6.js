function upperCase(str)
{
	var strArray=str.split(" ");
	for(let i=0;i<strArray.length;i++)
	{
		strArray[i]=strArray[i][0].toUpperCase()+strArray[i].substr(1);
	}
	
	return strArray.join(" ");
}

console.log(upperCase("abc pQr xyz"));
